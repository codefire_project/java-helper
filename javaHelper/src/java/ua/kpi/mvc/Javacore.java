/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ua.kpi.mvc;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nadia
 */
@Entity
@Table(name = "javacore", catalog = "helper", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Javacore.findAll", query = "SELECT j FROM Javacore j"),
    @NamedQuery(name = "Javacore.findById", query = "SELECT j FROM Javacore j WHERE j.id = :id"),
    @NamedQuery(name = "Javacore.findByTopic", query = "SELECT j FROM Javacore j WHERE j.topic = :topic")})
public class Javacore implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "topic", length = 255)
    private String topic;

    public Javacore() {
    }

    public Javacore(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Javacore)) {
            return false;
        }
        Javacore other = (Javacore) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ua.kpi.mvc.Javacore[ id=" + id + " ]";
    }
    
}
