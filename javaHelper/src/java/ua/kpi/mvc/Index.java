package ua.kpi.mvc;

import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ua.kpi.dao.DBController;

@Controller
public class Index {
    
    @Autowired
    private DBController dBController;
    
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView index() throws SQLException{
        ModelAndView view = new ModelAndView("info");
        view.addObject("data", dBController.getDBInfo());
        return view;
    }
    
    @RequestMapping(value = "/index/{param}", method = RequestMethod.GET)
    public ModelAndView index(@PathVariable("param") String param) throws SQLException{
        ModelAndView view = new ModelAndView("info");
        JdbcTemplate template = dBController.jdbcTempalte();
        view.addObject("info", dBController.getDBInfo(template, param));
        return view;
    }
}
