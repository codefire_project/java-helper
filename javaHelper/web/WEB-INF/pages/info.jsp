<%-- 
    Document   : info
    Created on : Apr 5, 2014, 11:59:14 AM
    Author     : CodeFire
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>Database info!!!!</h3>
        <c:forEach var="str" items="${data}">
            ${str}<br />
        </c:forEach>
        ${info}
    </body>
</html>
