<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
        <script type="text/javascript" src="scripts/shCore.js"></script>
        <script type="text/javascript" src="scripts/shBrushJava.js"></script>
        <link type="text/css" rel="stylesheet" href="styles/shCoreDjango.css"/>
        <script type="text/javascript">SyntaxHighlighter.all();</script>

        <link type="text/css" rel="stylesheet" href="welcomepage.css"/>


    </head>

    <body>

        <div id="wrapper">
            <div id="content">
                <div id="leftcolumn">
                    <table>
                        <tbody >
                            <tr style="border-bottom: 0px solid #eee;">
                                <td width="30%" valign="top">
                                    <h3 id="titles">
                                        Java
                                    </h3>
                                </td>
                                <td width="70%" valign="top">
                                    <h5 id="fontforlist">
                                        <a href="http://www.codefire.com.ua/kursy-programmirovanija-java-kiev">
                                            Java Tutorial
                                        </a>
                                    </h5>
                                    <h5 id="fontforlist">
                                        <a href="http://www.codefire.com.ua/kursy-programmirovanija-java-kiev">
                                            JavaCore
                                        </a>
                                    </h5>
                                    <h5 id="fontforlist">
                                        <a href="http://www.codefire.com.ua/kursy-programmirovanija-java-kiev">
                                            JDBC
                                        </a>
                                    </h5>
                                    <h5 id="fontforlist">
                                        <a href="http://www.codefire.com.ua/kursy-programmirovanija-java-kiev">
                                            Patterns
                                        </a>
                                    </h5>
                                    <h5 id="fontforlist">
                                        <a href="http://www.codefire.com.ua/kursy-programmirovanija-java-kiev">
                                            Android
                                        </a>
                                    </h5>
                                    <h5 id="fontforlist">
                                        <a href="http://www.codefire.com.ua/kursy-programmirovanija-java-kiev">
                                            HTML/CSS
                                        </a>
                                    </h5>
                                    <h5 id="fontforlist">
                                        <a href="http://www.codefire.com.ua/kursy-programmirovanija-java-kiev">
                                            JavaScript
                                        </a>
                                    </h5>
                                    <h5 id="fontforlist">
                                        <a href="http://www.codefire.com.ua/kursy-programmirovanija-java-kiev">
                                            Servlets
                                        </a>
                                    </h5>
                                    <h5 id="fontforlist">
                                        <a href="http://www.codefire.com.ua/kursy-programmirovanija-java-kiev">
                                            JSF 2
                                        </a>
                                    </h5>
                                    <h5 id="fontforlist">
                                        <a href="http://www.codefire.com.ua/kursy-programmirovanija-java-kiev">
                                            EJB 3
                                        </a>
                                    </h5>
                                    <h5 id="fontforlist">
                                        <a href="http://www.codefire.com.ua/kursy-programmirovanija-java-kiev">
                                            Spring
                                        </a>
                                    </h5>
                                    <h5 id="fontforlist">
                                        <a href="http://www.codefire.com.ua/kursy-programmirovanija-java-kiev">
                                            Hibernate
                                        </a>
                                    </h5>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="rightcolumn">
                    <form method="get" action="send_code">
                        <h3 id="titles">Add your own code</h3>

                        <textarea id="usercode" name="usercode">
                           
                        </textarea>
                        <br>
                        <br>
                        <input type="reset" value="clear">
                        <input type="submit" value="send">


                    </form>

                </div>
            </div>
            
            <div id="code">
                <h2>Example</h2>
                <pre  class="brush: java;">
                   
@Override
    public List<String> getDBInfo() throws SQLException{
        DriverManagerDataSource dataSource = context.getBean("dataSource", DriverManagerDataSource.class);
        List<String> data;
        try (Connection connect = dataSource.getConnection()) {
            DatabaseMetaData metaData = connect.getMetaData();
            data = new ArrayList<>();
            data.add(metaData.getDatabaseProductName());
            data.add(metaData.getDatabaseProductVersion());
        }
        return data;
    }</pre>
            </div>
            <div id="footer">
                <a href="http://www.codefire.com.ua">
                    codefire.com.ua
                </a>
                | Email:info at codefire.com.ua | © Demo Source and Support. All rights reserved.
            </div>
        </div>
    </body>
</html>
